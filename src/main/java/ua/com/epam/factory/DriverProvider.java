package ua.com.epam.factory;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;


public class DriverProvider {
    private static final Logger logger = LogManager.getLogger(DriverProvider.class);
    private static final ThreadLocal<WebDriver> DRIVER_POOL = new ThreadLocal<>();

    public static WebDriver getDriver() {
        if (DRIVER_POOL.get() == null) {
            DRIVER_POOL.set(DriverFactory.createDriver());
        }
        return DRIVER_POOL.get();
    }

    public static void quitDriver() {
        if (DRIVER_POOL.get() != null) {
            DRIVER_POOL.get().quit();
            DRIVER_POOL.remove();
            logger.info("Driver was successfully closed");
        }
    }

    private DriverProvider() {
    }
}
