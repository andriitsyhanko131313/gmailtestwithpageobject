package ua.com.epam.asserts;

import io.qameta.allure.Step;
import org.testng.Assert;
import ua.com.epam.ui.pages.DraftsPage;
import ua.com.epam.ui.pages.HomePage;
import ua.com.epam.utils.WaitUtils;


public class MessageAsserter {
    private final HomePage homePage;
    private final DraftsPage draftsPage;

    public MessageAsserter() {
        homePage = new HomePage();
        draftsPage = new DraftsPage();
    }

    @Step("Message with subject [{subject}] added to drafts")
    public void assertMessageAddedToDrafts(String subject) {
        WaitUtils.waitForVisibility(draftsPage.getMessageBySubject(subject));
        boolean actual = draftsPage.getMessageBySubject(subject).isDisplayed();
        Assert.assertTrue(actual, String.format("Expected message [%s], but found [%s]", actual, false));
    }

    @Step("Assert message: [{expectedText}]")
    public void assertMessageSent(String expectedText) {
        String actualText = homePage.getInformationMessage().waitTextToBe(expectedText).getText();
        Assert.assertEquals(actualText, expectedText,
                String.format("Expected message [%s], but found [%s]", expectedText, actualText));
    }
}
