package ua.com.epam.ui.actions;

import io.qameta.allure.Step;
import ua.com.epam.ui.pages.LoginPage;


public class LoginActions {
    private final LoginPage loginPage;

    public LoginActions() {
        loginPage = new LoginPage();
    }

    @Step("Log in to gmail account")
    public void logInToAccount(String login, String password) {
        loginPage.setLoginField(login);
        loginPage.setPasswordField(password);
    }
}
