package ua.com.epam.ui.pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.support.FindBy;
import ua.com.epam.decorator.elements.Input;

public class LoginPage extends AbstractPage {
    @FindBy(id = "identifierId")
    private Input inputLoginField;

    @FindBy(xpath = "//input[@type='password']")
    private Input inputPasswordField;

    public void setLoginField(String login) {
        inputLoginField.waitForFieldReadyToInput();
        inputLoginField.sendKeys(login, Keys.ENTER);
    }

    public void setPasswordField(String password) {
        inputPasswordField.waitForFieldReadyToInput();
        inputPasswordField.sendKeys(password, Keys.ENTER);
    }
}
