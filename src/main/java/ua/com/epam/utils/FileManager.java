package ua.com.epam.utils;

import ua.com.epam.configurations.PropertiesManager;

import java.util.List;

public class FileManager {
    public static List<User> getUsers() {
        return FileReader.readListOfObject(PropertiesManager.getUsersFilePath(), User.class);
    }

    public static Letter getLetter() {
        return FileReader.readObject(PropertiesManager.getLetterFilePath(), Letter.class);
    }

    private FileManager() {
    }
}
