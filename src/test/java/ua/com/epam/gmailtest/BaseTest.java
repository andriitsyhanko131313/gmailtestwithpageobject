package ua.com.epam.gmailtest;


import org.testng.annotations.AfterMethod;
import org.testng.annotations.Listeners;
import ua.com.epam.configurations.PropertiesManager;
import ua.com.epam.factory.DriverProvider;
import ua.com.epam.listener.GmailTestListener;
import ua.com.epam.utils.AllureAttachments;

import java.io.IOException;

@Listeners(GmailTestListener.class)
public class BaseTest {
    @AfterMethod
    public void tearDown() throws IOException {
        DriverProvider.quitDriver();
        AllureAttachments.addFileToAllure(PropertiesManager.getLogsFilePath());
    }
}
