package ua.com.epam.gmailtest;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ua.com.epam.asserts.LoginAsserter;
import ua.com.epam.asserts.MessageAsserter;
import ua.com.epam.ui.actions.MessageActions;
import ua.com.epam.ui.actions.LoginActions;
import ua.com.epam.ui.actions.NavigationActions;
import ua.com.epam.utils.FileManager;
import ua.com.epam.utils.Letter;
import ua.com.epam.utils.User;

import static ua.com.epam.constatnts.Constants.*;

public class SendMessageFromDraftsTest extends BaseTest {

    @DataProvider(parallel = true)
    public Object[] users() {
        return FileManager.getUsers().toArray();
    }

    @Test(description = "Verify that message is saved as draft and send message", dataProvider = "users")
    public void sendMessageFromDraftsTestCase(User user) {
        NavigationActions navigationAction = new NavigationActions();
        LoginActions loginActions = new LoginActions();
        MessageActions messageAction = new MessageActions();
        LoginAsserter loginAsserter = new LoginAsserter();
        MessageAsserter messageAsserter = new MessageAsserter();
        Letter letter = FileManager.getLetter();

        navigationAction.navigateToLoginPage();
        loginActions.logInToAccount(user.getLogin(), user.getPassword());
        loginAsserter.assertSuccessfulLogIn(user.getLogin());

        messageAction.fillInNewMessage(letter);
        messageAction.closeMessageForm();
        messageAction.openDrafts();
        messageAsserter.assertMessageAddedToDrafts(letter.getSubject());

        messageAction.openMessage(letter.getSubject());
        messageAction.sendMessage();
        messageAsserter.assertMessageSent(SUCCESSFULLY_SENT_MESSAGE);
    }
}
